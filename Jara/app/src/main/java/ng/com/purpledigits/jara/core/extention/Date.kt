package ng.com.purpledigits.jara.core.extention

import net.danlew.android.joda.DateUtils
import org.joda.time.LocalDate
import java.util.*
import java.util.concurrent.TimeUnit

fun Date.add(end: Date): Date = Date(
	time + end.time
)

fun Date.add(end: Int): Date = Date(
	time + TimeUnit.DAYS.toMillis(end.toLong())
)

fun Date.isToday(): Boolean {
	val today = LocalDate.fromDateFields(this)
	return DateUtils.isToday(today)
}