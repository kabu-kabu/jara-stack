package ng.com.purpledigits.jara.core.exception

sealed class Failure {
	class NetworkConnection: Failure()
	class ServerError: Failure()

	/** * Extend this class for feature specific failures.*/
	abstract class FeatureFailure: Failure()
}