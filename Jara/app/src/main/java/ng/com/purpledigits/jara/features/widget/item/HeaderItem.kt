package ng.com.purpledigits.jara.features.widget.item

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_header.*
import ng.com.purpledigits.jara.R

class HeaderItem(
	@StringRes private val titleStringResId: Int,
	@StringRes private val subtitleResId: Int? = null,
	@DrawableRes private val iconResId: Int? = null,
	private val onIconClickListener: View.OnClickListener? = null) : Item() {

	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.title.setText(titleStringResId)

		viewHolder.subtitle.apply {
			visibility = View.GONE
			subtitleResId?.let {
				visibility = View.VISIBLE
				setText(it)
			}
		}

		viewHolder.icon.apply {
			visibility = View.GONE
			iconResId?.let {
				visibility = View.VISIBLE
				setImageResource(it)
				setOnClickListener(onIconClickListener)
			}
		}
	}

	override fun getLayout(): Int = R.layout.item_header
}