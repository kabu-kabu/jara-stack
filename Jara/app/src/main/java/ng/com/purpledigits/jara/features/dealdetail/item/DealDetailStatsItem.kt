package ng.com.purpledigits.jara.features.dealdetail.item

import android.graphics.Paint
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.deal_details_stat.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.features.deal.Deal


class DealDetailStatsItem(private val deal: Deal): Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.old_price.apply {
			paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
		}
	}

	override fun getLayout(): Int  = R.layout.deal_details_stat
}