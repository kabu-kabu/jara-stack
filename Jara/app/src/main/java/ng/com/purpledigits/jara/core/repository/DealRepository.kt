package ng.com.purpledigits.jara.core.repository

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.functional.Either
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam
import ng.com.purpledigits.jara.features.deal.Deal
import ng.com.purpledigits.jara.features.deal.DealItem

interface DealRepository: Repository {
	fun getDeals(params: DealParam): Either<Failure, List<DealItem>>
	fun getRelatedDeals(params: RelatedDealParam): Either<Failure, List<Deal>>
}