package ng.com.purpledigits.jara.core.platform

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*
import ng.com.purpledigits.jara.R.id
import ng.com.purpledigits.jara.R.layout
import ng.com.purpledigits.jara.core.extention.inTransaction

abstract class BaseActivity : AppCompatActivity() {
	abstract fun fragment(): BaseFragment

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(layout.activity_layout)
		setSupportActionBar(toolbar)
		addFragment(savedInstanceState)
	}

	override fun onBackPressed() {
		(supportFragmentManager.findFragmentById(id.fragmentContainer) as BaseFragment).onBackPressed()
		super.onBackPressed()
	}

	private fun addFragment(savedInstanceState: Bundle?) =
		savedInstanceState ?: supportFragmentManager.inTransaction { add(id.fragmentContainer, fragment()) }
}