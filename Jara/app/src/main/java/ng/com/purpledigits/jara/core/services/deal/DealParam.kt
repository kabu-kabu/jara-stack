package ng.com.purpledigits.jara.core.services.deal

import ng.com.purpledigits.jara.features.deal.Deal

open class DealParam(open val limit: Int = 10, open val skip: Int = 0)

class RelatedDealParam(val deal: Deal, override val limit: Int = 10, override val skip: Int = 0): DealParam(limit, skip)