package ng.com.purpledigits.jara.core

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.text.style.ReplacementSpan


class RoundedBackgroundSpan(
	private val backgroundColor: Int,
	private val textColor: Int,
	private val mRect: RectF = RectF()
) : ReplacementSpan() {

	companion object {
		private const val CORNER_RADIUS: Float = 8F
		private const val PADDING = 25.0F
	}

	override fun draw(canvas: Canvas, text: CharSequence, start: Int, end: Int, x: Float, top: Int, y: Int, bottom: Int, paint: Paint) {
		// Background
		mRect.set(
			x,
			top.toFloat(),
			x + paint.measureText(text, start, end) + PADDING,
			bottom.toFloat()
		)
		paint.color = backgroundColor
		canvas.drawRoundRect(mRect, CORNER_RADIUS, CORNER_RADIUS, paint)

		// Text
		paint.color = textColor
		canvas.drawText(
			text,
			start,
			end,
			Math.round(x + (PADDING / 2)).toFloat(),
			((canvas.height / 2) - ((paint.descent() + paint.ascent()) / 2)),
			paint
		)
	}

	override fun getSize(paint: Paint, text: CharSequence?, start: Int, end: Int, fm: Paint.FontMetricsInt?): Int = Math.round(paint.measureText(text, start, end) + PADDING)
}