package ng.com.purpledigits.jara.features.auth

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import kotlinx.android.synthetic.main.auth_fragment_layout.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.services.auth.AuthService
import org.koin.android.ext.android.inject

class AuthFragment : Fragment() {

	val authenticator: AuthService by inject()
	lateinit var navController: NavController

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.auth_fragment_layout, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		navController = findNavController(view)

		if (!authenticator.isLoggedIn()) {
			animation_view.repeatCount = 20
			animation_view.playAnimation()
		}

		animation_view.addAnimatorListener(object : Animator.AnimatorListener {
			override fun onAnimationRepeat(animation: Animator?) {}

			override fun onAnimationEnd(animation: Animator) {
				Toast.makeText(activity!!, "Animation ended.", Toast.LENGTH_LONG).show()
				navController.navigate(AuthFragmentDirections.actionAuthFragmentToLoginFragment())
			}

			override fun onAnimationCancel(animation: Animator) {}

			override fun onAnimationStart(animation: Animator) {}

		})
	}
}