package ng.com.purpledigits.jara.core.services.deal

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.functional.Either
import retrofit2.Call

abstract class Network {
	fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
		return try {
			val response = call.execute()
			when (response.isSuccessful) {
				true -> Either.Right(transform((response.body() ?: default)))
				false -> Either.Left(Failure.ServerError())
			}
		} catch (exception: Throwable) {
			Either.Left(Failure.ServerError())
		}
	}
}
