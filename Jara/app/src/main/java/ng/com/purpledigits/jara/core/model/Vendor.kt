package ng.com.purpledigits.jara.core.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Vendor(
	val id: String,
	val title: String,
	val site: String,
	val phone: String,
	val logoUrl: String
): Parcelable