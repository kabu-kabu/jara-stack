package ng.com.purpledigits.jara.features.deal

data class DealItem(
	val deals: List<Deal> = mutableListOf(),
	val type: Int
)