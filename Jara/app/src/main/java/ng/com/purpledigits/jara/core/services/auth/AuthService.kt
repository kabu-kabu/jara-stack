package ng.com.purpledigits.jara.core.services.auth

interface AuthService {
	fun login(phone: String)
	fun isLoggedIn(): Boolean
	fun hasProfile(): Boolean
	fun setProfile(
		name: String,
		email: String,
		avatar: String
	)
	fun logout()
}