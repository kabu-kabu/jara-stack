package ng.com.purpledigits.jara.core.platform

import android.content.Context
import ng.com.purpledigits.jara.core.extention.networkInfo

/**
 * Injectable class which returns information about the network connection state.
 */

class NetworkHandler(private val context: Context) {
	val isConnected: Boolean? get() = context.networkInfo?.isConnectedOrConnecting
}