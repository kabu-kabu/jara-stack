package ng.com.purpledigits.jara.features.widget.item

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.column_item.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.Constants.INSET
import ng.com.purpledigits.jara.core.Constants.INSET_TYPE_KEY
import ng.com.purpledigits.jara.core.extention.loadSquaredFromUrl
import ng.com.purpledigits.jara.core.interactor.ItemInteractor
import ng.com.purpledigits.jara.features.deal.Deal

class ColumnItem(val deal: Deal, private val listener: ItemInteractor) : Item() {

	init {
		extras[INSET_TYPE_KEY] = INSET
	}

	override fun getId(): Long = deal.id.hashCode().toLong()

	override fun equals(other: Any?): Boolean {
		return when (other) {
			null -> false
			is ColumnItem -> {
				return this.id == other.id
			}
			else -> false
		}
	}

	override fun hashCode(): Int = deal.id.hashCode()

	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.column_cover_image.apply {
			loadSquaredFromUrl(deal.coverImage)
			setOnClickListener {
				listener.onItemClickListener(deal)
			}
			setOnLongClickListener {
				listener.onItemLongClickListener(deal)
				true
			}
		}
	}

	override fun getLayout(): Int = R.layout.column_item

	override fun getSpanSize(spanCount: Int, position: Int): Int = spanCount / 2
}