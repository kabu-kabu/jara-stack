package ng.com.purpledigits.jara.features.widget.item

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.qr_code_item_layout.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.features.deal.Deal

class DealQRCodeItem(private val deal: Deal): Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		try {
			val bitMatrix = MultiFormatWriter().encode(deal.id, BarcodeFormat.QR_CODE, 384, 384)
			val barcodeEncoder = BarcodeEncoder()
			val bitmap = barcodeEncoder.createBitmap(bitMatrix)
			viewHolder.qr_code_view.setImageBitmap(bitmap)
		} catch (e: WriterException) {
			e.printStackTrace()
		}
	}

	override fun getLayout(): Int = R.layout.qr_code_item_layout
}