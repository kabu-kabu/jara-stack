package ng.com.purpledigits.jara.features.deal

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.functional.Either
import ng.com.purpledigits.jara.core.interactor.UseCase
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam

class GetDeals(private val dealRepository: DealRepository): UseCase<List<DealItem>, DealParam>() {
	override suspend fun run(params: DealParam): Either<Failure, List<DealItem>> {
		return dealRepository.getDeals(params)
	}
}

class GetRelatedDeals(private val dealRepository: DealRepository): UseCase<List<Deal>, RelatedDealParam>() {
	override suspend fun run(params: RelatedDealParam): Either<Failure, List<Deal>> {
		return dealRepository.getRelatedDeals(params)
	}
}

