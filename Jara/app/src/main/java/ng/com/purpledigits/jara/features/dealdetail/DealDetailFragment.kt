package ng.com.purpledigits.jara.features.dealdetail


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.xwray.groupie.*
import com.xwray.groupie.groupiex.plusAssign
import kotlinx.android.synthetic.main.fragment_deal_detail.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.interactor.ItemInteractor
import ng.com.purpledigits.jara.features.deal.Deal
import ng.com.purpledigits.jara.features.deal.DealsViewModel
import ng.com.purpledigits.jara.features.deal.widgets.CardCarousel
import ng.com.purpledigits.jara.features.deal.widgets.featureddeal.FeatureItemViewItem
import ng.com.purpledigits.jara.features.deal.widgets.flashdeals.FlashDealItem
import ng.com.purpledigits.jara.features.dealdetail.item.DealDetailDescBodyItem
import ng.com.purpledigits.jara.features.dealdetail.item.DealDetailMetaItem
import ng.com.purpledigits.jara.features.dealdetail.item.DealDetailStatsItem
import ng.com.purpledigits.jara.features.widget.HeaderItemDecoration
import ng.com.purpledigits.jara.features.widget.item.DealQRCodeItem
import ng.com.purpledigits.jara.features.widget.item.HeaderItem
import org.koin.android.architecture.ext.viewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class DealDetailFragment : Fragment(), ItemInteractor {
	override fun onItemClickListener(item: Deal) {
		val direction = DealDetailFragmentDirections.actionDealDetailFragmentSelf(item)
		navController.navigate(direction)
	}

	override fun onItemLongClickListener(item: Deal) {}

	val vm: DealsViewModel by viewModel()

	private var deal: Deal? = null

	private val white: Int by lazy { ContextCompat.getColor(activity!!, R.color.white) }
	private val betweenPadding: Int by lazy { resources.getDimensionPixelSize(R.dimen.padding_small) }
	private lateinit var navController: NavController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val args = DealDetailFragmentArgs.fromBundle(arguments)
		deal = args.deal
		if (deal != null) {
			vm.loadRelatedDeals(deal!!)
		}
		setHasOptionsMenu(true)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_deal_detail, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		navController = Navigation.findNavController(view)
		val groupAdapter = GroupAdapter<ViewHolder>()
		groupAdapter.apply {
			setOnItemClickListener(onItemClickListener)
			setOnItemLongClickListener(onItemLongClickListener)
			spanCount = 12
		}
		val grid = GridLayoutManager(activity!!, groupAdapter.spanCount).apply {
			spanSizeLookup = groupAdapter.spanSizeLookup
		}

		groupAdapter += FeatureItemViewItem(deal!!, showDealMeta = false)
		groupAdapter += DealDetailMetaItem(deal!!)
		groupAdapter += DealDetailStatsItem(deal!!)
		groupAdapter += DealDetailDescBodyItem(deal!!.bodyText)
		groupAdapter += Section(HeaderItem(R.string.scan_to_redeem, null, iconResId = R.drawable.ic_qr_code)).apply {
			add(DealQRCodeItem(deal!!))
		}

		deal_detail_view.apply {
			layoutManager = grid
			addItemDecoration(HeaderItemDecoration(white, betweenPadding))
			adapter = groupAdapter
		}

		vm.relatedDeals.observe(this, Observer { deals ->
			Log.e("FRAG-DETAILS", "${deals.size}")
			val adapter = GroupAdapter<com.xwray.groupie.kotlinandroidextensions.ViewHolder>()

			for (deal in deals) {
				adapter.apply {
					add(FlashDealItem(deal, this@DealDetailFragment))
				}
			}

			groupAdapter.add(CardCarousel("Related deals", R.drawable.ic_discount, adapter))
		})
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		inflater.inflate(R.menu.deal_detail_menu, menu)
	}

	private val onItemClickListener = OnItemClickListener { _, _ ->

	}

	private val onItemLongClickListener = OnItemLongClickListener { _, _ ->
		return@OnItemLongClickListener true
	}

}
