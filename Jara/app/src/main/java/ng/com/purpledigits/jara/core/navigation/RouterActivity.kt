package ng.com.purpledigits.jara.core.navigation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import kotlinx.android.synthetic.main.activity_router_layout.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.R.id
import ng.com.purpledigits.jara.R.layout
import ng.com.purpledigits.jara.core.extention.gone
import ng.com.purpledigits.jara.core.extention.visible


class RouterActivity : AppCompatActivity(), NavController.OnNavigatedListener {

	lateinit var navController: NavController

	override fun onNavigated(controller: NavController, destination: NavDestination) {
		when (destination.label) {
			"DealListFragment" -> {
				supportActionBar?.setDisplayHomeAsUpEnabled(false)
				logo.visible()
			}
			else -> {
				showLogo()
			}
		}
	}

	private fun showLogo() {
		supportActionBar?.setDisplayHomeAsUpEnabled(true)
		supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_up_logo)
		logo.gone()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(layout.activity_router_layout)
		setSupportActionBar(toolbar)
		supportActionBar?.setDisplayShowTitleEnabled(false)
		navController = findNavController(this, id.nav_host_fragment)
		setupWithNavController(bottom_nav, navController)
		setupActionBarWithNavController(this, navController)
		navController.addOnNavigatedListener(this)
	}

	override fun onSupportNavigateUp(): Boolean =
		findNavController(this, id.nav_host_fragment).navigateUp()
}
