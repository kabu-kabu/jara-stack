package ng.com.purpledigits.jara.features.widget

import androidx.annotation.ColorInt
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.decoration.HeaderItemDecoration

class HeaderItemDecoration(@ColorInt background: Int, sidePaddingPixels: Int):
	HeaderItemDecoration(background, sidePaddingPixels, R.layout.item_header)