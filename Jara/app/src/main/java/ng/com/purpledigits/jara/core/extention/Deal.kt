package ng.com.purpledigits.jara.core.extention

import ng.com.purpledigits.jara.core.Constants.DEAL_FEATURED
import ng.com.purpledigits.jara.features.deal.Deal
import ng.com.purpledigits.jara.features.deal.DealItem
import java.util.*

fun DealItem.isFeaturedCarousel(): Boolean = type == DEAL_FEATURED && deals.size > 1 && !hasExpired()

fun DealItem.hasExpired(): Boolean {
	var hasExpired = false

	for (item in deals) {
		if (item.hasExpired()) {
			hasExpired = true
			break
		}
	}
	return hasExpired
}

fun Deal.hasExpired(): Boolean = endsAt.after(Date())