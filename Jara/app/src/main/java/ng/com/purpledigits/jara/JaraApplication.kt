package ng.com.purpledigits.jara

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid
import ng.com.purpledigits.jara.core.di.jaraAppModules
import org.koin.android.ext.android.startKoin

class JaraApplication : Application() {
	override fun onCreate() {
		super.onCreate()
		startKoin(this, jaraAppModules)
		JodaTimeAndroid.init(this)
	}
}