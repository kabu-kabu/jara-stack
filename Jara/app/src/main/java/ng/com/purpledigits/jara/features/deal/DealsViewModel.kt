package ng.com.purpledigits.jara.features.deal

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ng.com.purpledigits.jara.core.platform.BaseViewModel
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam

// TODO: make fine grain changes on returned items here for DiffUtil
class DealsViewModel(private val getDeals: GetDeals, private val getRelatedDeals: GetRelatedDeals) : BaseViewModel() {
	private val dealsMut: MutableLiveData<List<DealItem>> = MutableLiveData()
	private val moreDealsMut: MutableLiveData<List<Deal>> = MutableLiveData()
	private val relatedDealsMut: MutableLiveData<List<Deal>> = MutableLiveData()
	val deals: LiveData<List<DealItem>> = dealsMut
	val moreDeals: LiveData<List<Deal>> = moreDealsMut
	val relatedDeals: LiveData<List<Deal>> = relatedDealsMut
	private var dealParam: DealParam = DealParam(0, 0)

	fun loadDeals(params: DealParam) {
		if (params == dealParam) {
			return
		} else {
			dealParam = params
			getDeals(params) { it.either(::handleFailure, ::handleDealList) }
		}
	}

	fun loadMoreDeals(params: DealParam) {
		if (params == dealParam) {
			return
		} else {
			dealParam = params
			getDeals(params) { it.either(::handleFailure, ::handleMoreDealList) }
		}
	}

	fun loadRelatedDeals(deal: Deal) {
		Log.e("DETAILS", deal.id)
		getRelatedDeals(RelatedDealParam(deal)) { it.either(::handleFailure, ::handleRelatedDealsList)}
	}

	private fun handleRelatedDealsList(list: List<Deal>) {
		relatedDealsMut.value = list
	}

	private fun handleMoreDealList(list: List<DealItem>) {
		moreDealsMut.value = list.flatMap {
			it.deals
		}

	}

	private fun handleDealList(_deals: List<DealItem>) {
		dealsMut.value = _deals
	}
}