package ng.com.purpledigits.jara.features.widget.item

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_carousel.*
import ng.com.purpledigits.jara.R

class CarouselItem(private val carouselDecoration: RecyclerView.ItemDecoration,
                   private val carouselAdapter: GroupAdapter<ViewHolder>
) : Item() {
	override fun getLayout(): Int = R.layout.item_carousel

	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.recyclerView.apply {
			layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
			adapter = carouselAdapter

			// We don't know if the layout we're passed has been bound before so
			// we need to ensure we don't register the item decoration multiple times,
			// by trying to remove it first. (Nothing happens if it's not registered.)
			removeItemDecoration(carouselDecoration)
			addItemDecoration(carouselDecoration)
		}
	}

}