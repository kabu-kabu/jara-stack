package ng.com.purpledigits.jara.features.deal

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ng.com.purpledigits.jara.core.model.Vendor
import java.util.*
@Parcelize
data class Deal(
	val id: String,
	val currency: Int,
	val price: Double,
	val discount: Double,
	val title: String,
	val coverImage: String,
	val bodyText: String,
	val vendor: Vendor?,
	val createdAt: Date,
	val updatedAt: Date,
	val endsAt: Date
): Parcelable