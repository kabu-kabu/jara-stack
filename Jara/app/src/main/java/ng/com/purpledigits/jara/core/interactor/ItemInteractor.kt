package ng.com.purpledigits.jara.core.interactor

import ng.com.purpledigits.jara.features.deal.Deal

interface ItemInteractor {

	fun onItemClickListener(item: Deal)
	fun onItemLongClickListener(item: Deal)

}