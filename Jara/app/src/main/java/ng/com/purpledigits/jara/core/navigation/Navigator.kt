package ng.com.purpledigits.jara.core.navigation

import android.view.View
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigator
@Inject constructor() {
	class Extras(val transitionSharedElement: View)
}