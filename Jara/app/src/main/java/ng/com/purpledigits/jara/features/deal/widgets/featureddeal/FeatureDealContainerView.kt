package ng.com.purpledigits.jara.features.deal.widgets.featureddeal

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_carousel.*
import ng.com.purpledigits.jara.R

class FeatureDealContainerView(
	private val carouselAdapter: GroupAdapter<ViewHolder>
): Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {

		viewHolder.recyclerView.apply {
			layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
			adapter = carouselAdapter
			clearOnScrollListeners()
			onFlingListener = null
		}

		PagerSnapHelper().apply {
			attachToRecyclerView(viewHolder.recyclerView)
		}

		viewHolder.pageIndicator.apply {
			if (viewHolder.recyclerView.adapter?.itemCount!! > 0){
				attachTo(viewHolder.recyclerView)
			}
		}
	}

	override fun getLayout(): Int = R.layout.item_carousel
}
