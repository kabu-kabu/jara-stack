package ng.com.purpledigits.jara.features.deal.widgets.featureddeal

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View.GONE
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.feature_deal_item.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.RoundedBackgroundSpan
import ng.com.purpledigits.jara.core.extention.isToday
import ng.com.purpledigits.jara.core.extention.loadFromUrl
import ng.com.purpledigits.jara.core.interactor.ItemInteractor
import ng.com.purpledigits.jara.features.deal.Deal
import org.joda.time.DateTime
import org.joda.time.Days

class FeatureItemViewItem(
	private val deal: Deal,
	private val listener: ItemInteractor? = null,
	private val showDealMeta: Boolean = true
) : Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.feature_image.apply {
			loadFromUrl(deal.coverImage)
			setOnClickListener { _ ->
				listener?.onItemClickListener(deal)
			}
			setOnLongClickListener { _ ->
				listener?.onItemLongClickListener(deal)
				true
			}
		}

		if (showDealMeta) {
			viewHolder.discount_txt.apply {
				setDiscountText(this, deal)
			}
		} else {
			viewHolder.discount_txt.visibility = GONE
		}
	}

	private fun setDiscountText(textView: TextView, deal: Deal) {
		if (deal.endsAt.isToday()) {
			textView.text = textView.context.getString(R.string.ends_today_txt)
		} else {
			val now = DateTime.now()
			val endsAt = Days.daysBetween(now, DateTime().withMillis(deal.endsAt.time)).days


			val text = textView.context.resources.getQuantityString(R.plurals.discount_end_plurals, endsAt, endsAt)

			val backgroundColor = ContextCompat.getColor(textView.context, R.color.discount_span_bg)
			val textColor = ContextCompat.getColor(textView.context, R.color.colorTextPrimaryWhite)

			val ssBuilder = SpannableStringBuilder(text).apply {
				setSpan(
					RoundedBackgroundSpan(backgroundColor, textColor),
					text.indexOf("$endsAt"),
					text.indexOf("$endsAt") + "$endsAt".length,
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
				)
			}
			textView.text = ssBuilder
		}
	}

	override fun getLayout(): Int = R.layout.feature_deal_item
}