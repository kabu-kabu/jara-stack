package ng.com.purpledigits.jara.features.dealdetail.item

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.deal_detail_desc_body.*
import ng.com.purpledigits.jara.R

class DealDetailDescBodyItem(private val body: String): Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.body.text = body
	}

	override fun getLayout(): Int = R.layout.deal_detail_desc_body
}