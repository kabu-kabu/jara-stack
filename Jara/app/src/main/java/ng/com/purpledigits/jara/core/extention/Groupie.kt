package ng.com.purpledigits.jara.core.extention

import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder

operator fun GroupAdapter<ViewHolder>.plusAssign(element: Group) = this.updateAsync(mutableListOf(element))


operator fun GroupAdapter<ViewHolder>.plusAssign(groups: Collection<Group>) = this.updateAsync(groups.toMutableList())


operator fun GroupAdapter<ViewHolder>.minusAssign(element: Group) = this.remove(element)

