package ng.com.purpledigits.jara.features.deal.widgets

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.carded_carousel.*
import ng.com.purpledigits.jara.R

class CardCarousel(
	private val title: String,
	@DrawableRes private val icon: Int? = null,
	private val carouselAdapter: GroupAdapter<ViewHolder>
) : Item() {

	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.title.apply {
			text = title
		}

		viewHolder.icon.apply {
			visibility = GONE
			icon?.let {
				visibility = VISIBLE
				setImageResource(it)
			}
		}

		viewHolder.deals_list.apply {
			layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
			adapter = carouselAdapter
		}

		viewHolder.pageIndicator.attachTo(viewHolder.deals_list)
	}

	override fun getLayout(): Int = R.layout.carded_carousel
}