package ng.com.purpledigits.jara.core

object Constants {
	const val DEAL_FEATURED = 0
	const val INSET_TYPE_KEY = "inset_type"
	const val INSET = "inset"
}