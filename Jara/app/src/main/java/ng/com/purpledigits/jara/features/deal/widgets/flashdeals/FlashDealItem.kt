package ng.com.purpledigits.jara.features.deal.widgets.flashdeals

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.deal_item.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.extention.loadSquaredFromUrl
import ng.com.purpledigits.jara.core.interactor.ItemInteractor
import ng.com.purpledigits.jara.features.deal.Deal

class FlashDealItem(private val deal: Deal, private val listener: ItemInteractor) : Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.deal_title.text = deal.title
		viewHolder.cover_image.apply {
			loadSquaredFromUrl(deal.coverImage)
			setOnClickListener {
				listener.onItemClickListener(deal)
			}
			setOnLongClickListener {
				listener.onItemLongClickListener(deal)
				true
			}
		}
	}

	override fun getLayout(): Int = R.layout.deal_item
}