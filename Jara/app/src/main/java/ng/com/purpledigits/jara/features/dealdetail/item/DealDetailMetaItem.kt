package ng.com.purpledigits.jara.features.dealdetail.item

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.deal_detail_meta_layout.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.extention.cropCircle
import ng.com.purpledigits.jara.core.extention.isToday
import ng.com.purpledigits.jara.features.deal.Deal
import org.joda.time.DateTime
import org.joda.time.Days

class DealDetailMetaItem(val deal: Deal) : Item() {
	override fun bind(viewHolder: ViewHolder, position: Int) {
		viewHolder.vendor_logo.apply {
			if (deal.vendor != null) {
				cropCircle(deal.vendor.logoUrl)
			} else {
				setImageResource(R.drawable.ic_jara_logo)
			}
		}

		viewHolder.title.text = deal.title

		viewHolder.meta.apply {
			text = if (deal.endsAt.isToday()) {
				context.getString(R.string.ends_today_txt)
			} else {
				val now = DateTime.now()
				val endsAt = Days.daysBetween(now, DateTime().withMillis(deal.endsAt.time)).days
				context.resources.getQuantityString(R.plurals.discount_end_plurals, endsAt, endsAt)
			}
		}
	}

	override fun getLayout(): Int = R.layout.deal_detail_meta_layout
}