package ng.com.purpledigits.jara.core.di

import ng.com.purpledigits.jara.features.deal.DealListFragment
import ng.com.purpledigits.jara.features.dealdetail.DealDetailFragment
import org.koin.dsl.module.applicationContext

/**
 *  Koin main module
 */

val JaraAppModule = applicationContext {
	factory { DealListFragment() }
	factory { DealDetailFragment() }
}

val jaraAppModules = listOf(RepoModule, JaraAppModule)