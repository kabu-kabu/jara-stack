package ng.com.purpledigits.jara.features.deal


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.*
import kotlinx.android.synthetic.main.fragment_deal_list.*
import ng.com.purpledigits.jara.R
import ng.com.purpledigits.jara.core.Constants.DEAL_FEATURED
import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.extention.plusAssign
import ng.com.purpledigits.jara.core.interactor.ItemInteractor
import ng.com.purpledigits.jara.core.services.auth.AuthService
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.features.deal.widgets.CardCarousel
import ng.com.purpledigits.jara.features.deal.widgets.featureddeal.FeatureDealContainerView
import ng.com.purpledigits.jara.features.deal.widgets.featureddeal.FeatureItemViewItem
import ng.com.purpledigits.jara.features.deal.widgets.flashdeals.FlashDealItem
import ng.com.purpledigits.jara.features.widget.HeaderItemDecoration
import ng.com.purpledigits.jara.features.widget.InfiniteScrollListener
import ng.com.purpledigits.jara.features.widget.item.ColumnItem
import ng.com.purpledigits.jara.features.widget.item.HeaderItem
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.inject

private const val SKIP = 10

/**
 * Deals List [Fragment] subclass.
 *
 */
class DealListFragment : Fragment(), ItemInteractor {

	val vm: DealsViewModel by viewModel()
	private val authenticator: AuthService by inject()

	private lateinit var navController: NavController
	private lateinit var groupAdapter: GroupAdapter<ViewHolder>
	private var sections = mutableListOf<Group>()
	private val white: Int by lazy { ContextCompat.getColor(activity!!, R.color.white) }
	private val betweenPadding: Int by lazy { resources.getDimensionPixelSize(R.dimen.padding_small) }
	private lateinit var searchView: SearchView
	private lateinit var queryTextListener: SearchView.OnQueryTextListener

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		vm.loadDeals(DealParam())
		setHasOptionsMenu(true)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.fragment_deal_list, container, false)
		val dealList = view.findViewById<RecyclerView>(R.id.deal_list)
		setupList(dealList)
		setupObservers()
		return view
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		if (!authenticator.isLoggedIn()) {
			navController = Navigation.findNavController(view)
		} else {
			navController.navigate(R.id.action_dealListFragment_to_authFragment)
		}
	}


	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		inflater.inflate(R.menu.deal_list_menu, menu)
		val searchItem = menu.findItem(R.id.action_search)
		val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

		searchView = searchItem.actionView!! as SearchView
		searchView.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
		queryTextListener = object : SearchView.OnQueryTextListener {
			override fun onQueryTextSubmit(query: String): Boolean {
				Log.i("onQueryTextChange", query)
				return true
			}

			override fun onQueryTextChange(newText: String): Boolean {
				Log.i("onQueryTextChange", newText)
				return true
			}
		}
		searchView.setOnQueryTextListener(queryTextListener)
		super.onCreateOptionsMenu(menu, inflater)
	}

	override fun onItemClickListener(item: Deal) {
		val detailDirections = DealListFragmentDirections.ActionDealListFragmentToDealDetailFragment(item)
		navController.navigate(detailDirections)
	}

	override fun onItemLongClickListener(item: Deal) {
		Toast.makeText(activity!!, item.title, Toast.LENGTH_LONG).show()
	}

	override fun onViewStateRestored(savedInstanceState: Bundle?) {
		super.onViewStateRestored(savedInstanceState)
		val savedRecyclerLayoutState: Parcelable? = savedInstanceState?.getParcelable(BUNDLE_RECYCLER_LAYOUT)
		deal_list.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		if (deal_list != null) {
			outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, deal_list.layoutManager?.onSaveInstanceState())
		}
	}

	private fun setupList(dealList: RecyclerView) {

		groupAdapter = GroupAdapter()

		groupAdapter.apply {
			setOnItemClickListener(onItemClickListener)
			setOnItemLongClickListener(onItemLongClickListener)
			spanCount = 12
		}

		val grid = GridLayoutManager(activity!!, groupAdapter.spanCount).apply {
			spanSizeLookup = groupAdapter.spanSizeLookup
		}

		dealList.apply {
			layoutManager = grid
			addItemDecoration(HeaderItemDecoration(white, betweenPadding))
			adapter = groupAdapter
			addOnScrollListener(object : InfiniteScrollListener(grid) {
				override fun onLoadMore(current_page: Int) {
					val skip = SKIP + (SKIP * current_page)
					val dealParam = DealParam(skip = skip)
					vm.loadMoreDeals(dealParam)
				}
			})
		}
	}

	private fun setupObservers() {
		vm.failure.observe(this, Observer {
			when (it) {
				is Failure.ServerError -> Log.e("ERROR", "Cannot reach server")
				is Failure.NetworkConnection -> Log.e("ERROR", "Check your network connection")
			}
		})
		vm.deals.observe(this, Observer {
			populateAdapter(it)
		})
		vm.moreDeals.observe(this, Observer {
			it.map { deal ->
				ColumnItem(deal, this@DealListFragment)
			}.map { dealItem ->
				sections.add(dealItem)
				groupAdapter += sections
			}
		})
	}

	private fun populateAdapter(list: List<DealItem>) {
		groupAdapter.clear()
		sections.clear()
		carouselSection(list)
		flashSection(list)
		updatingSection(list)
		if (groupAdapter.itemCount == 0)
			groupAdapter += sections
	}

	private fun updatingSection(list: List<DealItem>) {
		sections.add(HeaderItem(R.string.discounts_for_you_header_txt, R.string.discounts_for_you_sub_header_txt, R.drawable.ic_coupon))
		val deal = list.map {
			it.deals
		}.flatMap {
			it.map { deal ->
				ColumnItem(deal, this@DealListFragment)
			}
		}
		for (d in deal) {
			sections.add(d)
		}
	}

	private fun flashSection(list: List<DealItem>) {
		val flash = addFlashDeals(list)
		sections.add(flash)
	}

	private fun carouselSection(list: List<DealItem>) {
		val carousel = makeFeatureCarousel(list)
		sections.add(carousel)
	}

	private fun addFlashDeals(list: List<DealItem>): Group {
		val deals = mutableListOf<Deal>()
		for (l in list) {
			deals.addAll(l.deals)
		}
		val adapter = GroupAdapter<com.xwray.groupie.kotlinandroidextensions.ViewHolder>()

		for (deal in deals) {
			adapter.apply {
				add(FlashDealItem(deal, this@DealListFragment))
			}
		}

		return CardCarousel("Flash deals", R.drawable.ic_fire, adapter)
	}

	private fun makeFeatureCarousel(list: List<DealItem>): Group {
		val deals = list
			.filter {
				it.type == DEAL_FEATURED && it.deals.size > 1
			}
			.take(1)
			.map {
				it.deals
			}.flatMap {
				it
			}

		val adapter = GroupAdapter<com.xwray.groupie.kotlinandroidextensions.ViewHolder>()

		for (i in deals) {
			adapter.apply {
				add(FeatureItemViewItem(i, this@DealListFragment))
			}
		}

		return FeatureDealContainerView(adapter)
	}

	private val onItemClickListener = OnItemClickListener { item, _ ->

	}

	private val onItemLongClickListener = OnItemLongClickListener { item, _ ->
		return@OnItemLongClickListener true
	}

	companion object {
		const val BUNDLE_RECYCLER_LAYOUT = "recycler_state"
	}

}
