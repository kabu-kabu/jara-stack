package ng.com.purpledigits.jara.features.deal

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.functional.Either
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam

class DealRepositoryImpl(private val network: DealNetwork) : DealRepository {
	override fun getRelatedDeals(params: RelatedDealParam): Either<Failure, List<Deal>> {
		return network.getRelatedDeals(params)
	}

	override fun getDeals(params: DealParam): Either<Failure, List<DealItem>> {
		return network.getDeals(params)
	}

}