package ng.com.purpledigits.jara.core.di

import ng.com.purpledigits.jara.core.platform.NetworkHandler
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.core.services.AuthServiceImpl
import ng.com.purpledigits.jara.core.services.DealService
import ng.com.purpledigits.jara.core.services.auth.AuthService
import ng.com.purpledigits.jara.features.deal.*
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

val RepoModule = applicationContext {
	bean {
		NetworkHandler(get())
	}
	bean {
		DealRepositoryImpl(get()) as DealRepository
	}

	bean {
		AuthServiceImpl() as AuthService
	}

	bean {
		DealService()
	}

	bean {
		DealNetwork(get(), get())
	}

	bean {
		GetDeals(get())
	}

	bean {
		GetRelatedDeals(get())
	}

	viewModel {
		DealsViewModel(get(), get())
	}

}