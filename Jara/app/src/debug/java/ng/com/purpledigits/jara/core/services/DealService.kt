package ng.com.purpledigits.jara.core.services

import ng.com.purpledigits.jara.core.Constants.DEAL_FEATURED
import ng.com.purpledigits.jara.core.extention.add
import ng.com.purpledigits.jara.core.extention.random
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam
import ng.com.purpledigits.jara.features.deal.Deal
import ng.com.purpledigits.jara.features.deal.DealItem
import java.util.*

class DealService : Service {
	fun getDeals(params: DealParam): List<DealItem> = getLocalDeals(params.limit)


	private fun getLocalDeals(count: Int): List<DealItem> {
		val list = mutableListOf<DealItem>()

		repeat(count) {
			val noOfItems = (0..4).random()
			list.add(makeDealItem(it, noOfItems))
		}

		return list
	}

	private fun makeDealItem(index: Int, noOfItems: Int): DealItem {
		val items = mutableListOf<Deal>()
		repeat(noOfItems) {
			items.add(makeDeal("_deal_$index-$it-${Date().time}"))
		}
		return DealItem(
			deals = items,
			type = (DEAL_FEATURED..2).random()
		)
	}

	private fun makeDeal(id: String): Deal = Deal(
		id = id,
		bodyText = "$ipsum $id",
		createdAt = Date(),
		updatedAt = Date(),
		currency = 0,
		discount = 10.00,
		price = 200.00,
		endsAt = Date().add((2..8).random()),
		title = getString((0 until titles.size).random(), titles),
		vendor = null,
		coverImage = getString((0 until imgs.size).random(), imgs)
	)

	private fun getString(index: Int, array: Array<String>): String {
		return if (index > array.size) {
			array[0]
		} else {
			array[index]
		}
	}

	fun getRelatedDeals(params: RelatedDealParam): List<Deal> {
		val items = mutableListOf<Deal>()
		repeat(params.limit) {
			items.add(makeDeal("_deal_${params.deal.id}-$it-${Date().time}"))
		}
		return items
	}

	private val titles = arrayOf(
		"Black Friday bumper Sales!",
		"20% Cash Back at Five Roses Pub",
		"Haircut, Full-Highlights, or Classic Balayage Package at Charles Ifergan (Up to 57% Off)",
		"Spray of Sunshine",
		"20% Cash Back at Forever Yogurt",
		"Men's Marled Pullover Hoodie",
		"Studio Movie Grill",
		"2-in-1 Gravity Car Air Vent Windshield Dash Smartphone Mount Holder - Black",
		"Gino Pheroni Calvin Men's Chelsea Boots",
		"Stainless Steel Dumpling and Ravioli Maker"
	)

	private val imgs = arrayOf(
		"https://images.pexels.com/photos/788662/pexels-photo-788662.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/267401/pexels-photo-267401.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/154149/pexels-photo-154149.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/6253/city-street-typography-design.jpg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/266246/pexels-photo-266246.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/630839/pexels-photo-630839.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/1027393/pexels-photo-1027393.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/877701/pexels-photo-877701.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/103124/pexels-photo-103124.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/1191503/pexels-photo-1191503.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/1183986/pexels-photo-1183986.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/849835/pexels-photo-849835.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853",
		"https://images.pexels.com/photos/1350419/pexels-photo-1350419.jpeg?dl&fit=crop&crop=entropy&w=1280&h=853"
	)

	private val ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Netus et malesuada fames ac turpis egestas integer. Venenatis tellus in metus vulputate. Mauris sit amet massa vitae tortor condimentum lacinia quis. Mi bibendum neque egestas congue quisque egestas. Tristique risus nec feugiat in fermentum posuere urna. Sed euismod nisi porta lorem mollis aliquam."

}