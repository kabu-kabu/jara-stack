package ng.com.purpledigits.jara.features.deal

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.exception.Failure.NetworkConnection
import ng.com.purpledigits.jara.core.functional.Either
import ng.com.purpledigits.jara.core.platform.NetworkHandler
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.core.services.DealService
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.Network
import ng.com.purpledigits.jara.core.services.deal.RelatedDealParam

class DealNetwork (private val networkHandler: NetworkHandler, private val service: DealService) : Network(), DealRepository {
	override fun getRelatedDeals(params: RelatedDealParam): Either<Failure, List<Deal>> {
		return when (networkHandler.isConnected) {
			true -> Either.Right(service.getRelatedDeals(params))
			false -> Either.Left(NetworkConnection())
			null -> Either.Left(NetworkConnection())
		}
	}

	override fun getDeals(params: DealParam): Either<Failure, List<DealItem>> {
		return when (networkHandler.isConnected) {
			true -> Either.Right(service.getDeals(params))
			false -> Either.Left(NetworkConnection())
			null -> Either.Left(NetworkConnection())
		}
	}

}