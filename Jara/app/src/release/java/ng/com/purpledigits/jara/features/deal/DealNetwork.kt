package ng.com.purpledigits.jara.features.deal

import ng.com.purpledigits.jara.core.exception.Failure
import ng.com.purpledigits.jara.core.exception.Failure.NetworkConnection
import ng.com.purpledigits.jara.core.functional.Either
import ng.com.purpledigits.jara.core.platform.NetworkHandler
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.core.services.DealService
import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.core.services.deal.Network

class DealNetwork(private val networkHandler: NetworkHandler, private val service: DealService) : Network(), DealRepository {
	override fun getDeals(params: DealParam): Either<Failure, List<DealItem>> {
		return when (!networkHandler.isConnected) {
			true -> request(service.getDeals(params), { it }, emptyList())
			false -> Either.Left(NetworkConnection())
		}
	}
}