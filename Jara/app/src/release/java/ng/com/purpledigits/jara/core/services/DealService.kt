package ng.com.purpledigits.jara.core.services

import ng.com.purpledigits.jara.core.services.deal.DealParam
import ng.com.purpledigits.jara.features.deal.DealItem
import retrofit2.Call
import retrofit2.http.GET

interface DealService: Service {
	@GET fun getDeals(params: DealParam): Call<List<DealItem>>
}