package ng.com.purpledigits.jara.core.di

import ng.com.purpledigits.jara.core.platform.NetworkHandler
import ng.com.purpledigits.jara.core.repository.DealRepository
import ng.com.purpledigits.jara.features.deal.DealNetwork
import ng.com.purpledigits.jara.features.deal.DealRepositoryImpl
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

val RepoModule = applicationContext {
		bean {
			NetworkHandler(get())
		}
		bean {
			DealRepositoryImpl(get()) as DealRepository
		}

		bean {
			DealNetwork(get(), get())
		}
}